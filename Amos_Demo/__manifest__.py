# -*- coding: utf-8 -*-
# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
# odoo16
# QQ:35350428
# 邮件:35350428@qq.com
# 手机：13584935775
# 作者：'Amos'
# 公司网址： www.xodoo.cn
# Copyright 昆山一百计算机有限公司
# 日期：2023-09-16
# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

{
    'name': 'odoo 基本功能教学',
    'version': '1.0',
    'category': '中台应用/教学',
    'summary': '功能说明',
    'sequence': 0,
    'author': 'Amos',
    'website': 'http://xodoo.cn',
    'license': 'LGPL-3',
    'depends': ['base','mail', 'workflow'],
    'data': [
        'security/ir.model.access.csv',
        'data/amos_demo_data.xml',
        'views/amos_demo.xml',
        # 'views/amos_a.xml',
        # 'report/report.xml',
    ],
    'demo': [
    ],
    'price': '10',
    'currency': 'EUR',
    'installable': True,
    'application': True,
    'auto_install': False,
    'description': """
    odoo 基本功能教学
    """,
}
