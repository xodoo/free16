# -*- coding: utf-8 -*-
# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
# odoo16
# QQ:35350428
# 邮件:35350428@qq.com
# 手机：13584935775
# 作者：'Amos'
# 公司网址： www.xodoo.cn
# Copyright 昆山一百计算机有限公司
# 日期：2023-09-16
# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

{
    'name': '可视化-工作流引擎',
    'summary': '设计，测试，执行一体化',
    'category': '中台应用/个性化',
    'sequence': 10001,
    'author': 'Amos',
    'website': 'http://www.xodoo.cn',
    'depends': ['hr'],
    'version': '1.0',
    'data': [
        'security/ir.model.access.csv',
        'data/mail_activity_data.xml',
        'views/workflow.xml',
        'views/workflow_approval.xml',
        'views/workflow_info.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': True,
    'license': 'LGPL-3',
    'description': """默认提供静态参数""",
}
