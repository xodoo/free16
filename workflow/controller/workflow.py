# -*- coding: utf-8 -*-
# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
# odoo16
# QQ:35350428
# 邮件:35350428@qq.com
# 手机：13584935775
# 作者：'Amos'
# 公司网址： www.xodoo.cn
# Copyright 昆山一百计算机有限公司
# 日期：2023-09-16
# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

import os
from jinja2 import Environment, FileSystemLoader
from odoo import api, fields, models, tools, SUPERUSER_ID, _, Command
from odoo.api import call_kw

# TODO(amos): 加载模板
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
folder_path = BASE_DIR + "/static/templates"  # 相对文件路径
templateLoader = FileSystemLoader(searchpath=folder_path)
env = Environment(loader=templateLoader)
# TODO(amos): 去空格
env.trim_blocks = True
env.lstrip_blocks = True
env.cache_size = 5000  # 默认缓存模板数量
#::::定义全局变量
env.globals.update({
    'layout': 'false',  # 模板出现class_bg，默认模板
    'layout_nav_fixed': 'true',  # 固定侧导航
    'layout_header_fixed': 'true',  # 固定导航条
    'layout_header_color': 'bg-white',  # 导航背景
    'layout_sidenav_color': 'bg-white',  # 侧导航背景
    'layout_footer_color': 'bg-white',  # 页脚背景
})

# TODO(amos): 提供更多格式化方法
env.filters['date_format'] = tools.jinja_filters.date_format
env.filters['reverse'] = tools.jinja_filters.reverse_filter
env.filters['section_filter'] = tools.jinja_filters.section_filter
env.filters['date_filter'] = tools.jinja_filters.date_filter
env.filters['decode'] = tools.jinja_filters.decode
env.filters['split'] = tools.jinja_filters.split
env.filters['list_split'] = tools.jinja_filters.list_split

import logging

_logger = logging.getLogger(__name__)

import odoo
from odoo import http
from odoo.http import request
import json


class workflow(http.Controller):

    @http.route(['/workflow/my_approval'], type='http', auth="user", cors="*", csrf=False)
    def workflow_my_approval(self, lang="zh_CN", **kw):
        """
        我的审批
        :param kw:
        :url
        :return:
        """
        # TODO(amos): 检查session是否过期
        if not request.session.uid:
            return request.redirect('/web/login')
        # TODO(amos): URL重定向
        if kw.get('redirect'):
            return request.redirect(kw.get('redirect'), 303)
        cr, uid, context, pool = request.cr, request.session.uid, request.context, request.env

        print(request.session.uid)

        # TODO(amos): 参数化准备
        values = {}
        values['model'] = kw.get("model", "")  # TODO(amos): 那一个对象
        values['id'] = kw.get("id", "")  # TODO(amos): 表单ID

        obj = pool[values['model']].browse(int(values['id']))

        values["name"] = '审核'
        values["type"] = 'object'
        values["workflow_look"] = str(obj.workflow_look)
        values["context"] = {'wkf': 'workflow_ok,workflow_no','type_flows': 'button'}

        print(values)

        return json.dumps(values)



    @http.route(['/workflow/create/default_get'], type='http', auth="user", cors="*", csrf=False)
    def workflow_create_default_get(self, lang="zh_CN", **kw):
        """
        创建工作流程时的默认值
        :param kw:
        :url例子    http://127.0.0.1:9099/workflow/create/default_get?model=amos.demo&id=15
        :url
        :return:
        """
        cr, uid, context, pool = request.cr, request.session.uid, request.context, request.env

        # TODO(amos): 参数化准备
        values = {}
        values['session_id'] = request.session.sid
        values['model'] = kw.get("model", "")  # TODO(amos): 那一个对象
        values['id'] = kw.get("id", "")  # TODO(amos): 表单ID

        obj = pool[values['model']].browse(int(values['id'])).button_workflow_approval()

        context = dict(obj or {})

        context['call_workflow'] = True #增加第三方调用接口
        workflow_approval = pool['workflow.approval'].with_context(context).default_get(['name', 'is_yes', 'is_no', 'is_process_node', 'form_state', 'users_ids', 'type', 'check_user_ids', 'check_user_count', 'my_tips', 'warning'])

        return json.dumps(workflow_approval)


    @http.route(['/workflow/workflow_approval/but_confirm'], type='http', auth="user", cors="*", csrf=False)
    def workflow_workflow_approval_default_get(self, lang="zh_CN", **kw):
        """
        工作流弹出窗口确认
        :param kw:
        :url
        :return:
        """
        cr, uid, context, pool = request.cr, request.session.uid, request.context, request.env

        # TODO(amos): 参数化准备
        values = {}
        values['session_id'] = request.session.sid
        values['csrf_token'] = request.csrf_token()


        data = {'type': 'ir.actions.act_window',
                'res_model': 'workflow.approval',
                'view_mode': 'form', 'views': [(373, 'form')],
                'target': 'new', 'res_id': False,
                'context': {'lang': 'zh_CN',
                            'tz': 'Asia/Shanghai',
                            'uid': 7, 'one': 'one',
                            'process_id': 2,
                            'workflow_user_ids': [6, 7],
                            'default_check_user_ids': [(0, 0, {'user_id': 6, 'sequence': 1}), (0, 0, {'user_id': 7, 'sequence': 3})],
                            'mult_confirm': False,
                            'workflow_id': 78, 'last_user':
                                True, 'workflow_type': '并签',
                            'stop_flow': False},
                'flags': {'form': {'action_buttons': True}}, 'call_workflow': True}

        #::::界面上的值 保存到 workflow.approval
        values = {
            'name': '',   #原因
            'is_process_node': '', #审批中间过程
            'user_id': '', #用户
            'model': '', #对象
            'form_state': '', #从状态
            'to_state': '', #到状态
            'users_ids': '', #消息通知人
            'code': '', #自定义代码
            'type': '', #类型
            'state': '', #审批类型
            'check_user_ids': '', #审核人
            'my_tips': '', #提示
            'warning': '', #提醒：请添加下级审批人
        }
        approval = pool['workflow.approval'].sudo().create(values)

        #::::保存后执行
        context1 = {}
        context1['approval_state'] = '同意'
        context1['message'] = '同意'
        context1['message_type'] = '同意'
        obj = approval.with_context(context1).but_confirm()

        data = {}
        data['obj'] = str(obj)
        return json.dumps(data)
